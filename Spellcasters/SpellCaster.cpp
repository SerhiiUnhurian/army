#include "SpellCaster.h"
#include "../Spell/SpellBook.h"
#include "../Spell/Spell.h"
#include "../States/State.h"
#include "../States/MagicState.h"
#include "../Units/Unit.h"
#include "../Units/Exceptions.h"


SpellCaster::SpellCaster(const std::string& name, State* state, MagicState* magicState, Attack* attack, Ability* ability)
	: Unit(name, state, attack, ability), m_magicState(magicState), m_spellBook(SpellBook::getSpellBook())
{
}

SpellCaster::~SpellCaster() {
	delete m_magicState;
	delete m_spellBook; 
}

const SpellBook* SpellCaster::getSpellBook() const {
	if ( !m_spellBook )	{ throw NotAllowedException(); } // Where to catch?
	return m_spellBook;
}

void SpellCaster::setState(State* state) {
	Unit::setState(state);

	delete m_magicState;
	delete m_spellBook; 

	m_magicState = nullptr;
	m_spellBook = nullptr;
}

void SpellCaster::cast(SpellName spell, Unit* target) {
	ensureIsAlive();

	if ( !m_magicState ) { throw NotAllowedException(); } // Where to catch?

	try {
		Spell *concreteSpell = m_spellBook->getSpell(spell);
		m_magicState->reduceMana(concreteSpell->getCost());
		concreteSpell->apply(target, m_magicState);
	} catch(std::out_of_range error) {
		std::cout << "No such spell in spell book!" << std::endl;
	}
}

std::ostream& SpellCaster::print(std::ostream& out) const {
	Unit::print(out);
	if ( m_magicState ) {
		out << ", " << m_magicState;
	}

	return out;
}
