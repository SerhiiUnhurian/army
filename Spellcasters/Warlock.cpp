#include "Warlock.h"
#include "SpellCaster.h"
#include "../Units/Properties.h"
#include "../States/State.h"
#include "../States/MagicState.h"
#include "../Ability/WarlockAbility.h"
#include "../Attacks/Attack.h"

Warlock::Warlock(const std::string name, UnitType unitType) 
	: SpellCaster(name, new State(unitType, HP_WARLOCK, DMG_WARLOCK, false), new MagicState(MANA_WARLOCK, true), 
		new Attack(this), new WarlockAbility(this))
{
}

Warlock::~Warlock() {}

Demon* Warlock::getDemon() const {
	WarlockAbility* warlockAbility = dynamic_cast<WarlockAbility*>(m_ability);
	
	warlockAbility->getDemon();
}