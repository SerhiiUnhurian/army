#include "Priest.h"
#include "SpellCaster.h"
#include "../Units/Properties.h"
#include "../States/State.h"
#include "../States/MagicState.h"
#include "../Attacks/PriestAttack.h"


Priest::Priest(const std::string name, UnitType unitType) 
	: SpellCaster(name, new State(unitType, HP_PRIEST, DMG_PRIEST, false), new MagicState(MANA_PRIEST, false), new PriestAttack(this))
{
}

Priest::~Priest() {}