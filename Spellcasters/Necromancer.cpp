#include "Necromancer.h"
#include "SpellCaster.h"
#include "../Units/Properties.h"
#include "../States/State.h"
#include "../States/MagicState.h"
#include "../Attacks/NecromancerAttack.h"
// #include "../States/NecromancerState.h"

Necromancer::Necromancer(const std::string name, UnitType unitType) 
	: SpellCaster(name, new State(unitType, HP_NECROMANCER, DMG_NECROMANCER, true), new MagicState(MANA_NECROMANCER, true), 
		new NecromancerAttack(this))
{
}

Necromancer::~Necromancer() {
	auto it = m_observables.begin();

	while ( it != m_observables.end() ) {
		(*it)->unsubscribe(this);
		it++;
	}
}

void Necromancer::cast(SpellName spell, Unit* target) {
	attach(target);

	SpellCaster::cast(spell, target);
}

std::unordered_set<Unit*>& Necromancer::getObservables() {
	return m_observables;
}

void Necromancer::setState(State* state) {
	SpellCaster::setState(state);

	if ( !m_observables.empty() ) {
		auto it = m_observables.begin();

		while ( it != m_observables.end() ) {
			(*it)->unsubscribe(this);
			it++;
		}

		m_observables.clear();
	}
}

// void Necromancer::takeDamage(int dmg) {
// 	Unit::takeDamage(dmg);

// 	if ( !isAlive() && !m_observables.empty() ) {
// 		auto it = m_observables.begin();

// 		while ( it != m_observables.end() ) {
// 			(*it)->unsubscribe(this);
// 			it++;
// 		}
// 	}
// }

// void Necromancer::takeMagicDamage(int dmg) {
// 	Unit::takeMagicDamage(dmg);

// 	if ( !isAlive() && !m_observables.empty() ) {
// 		auto it = m_observables.begin();

// 		while ( it != m_observables.end() ) {
// 			(*it)->unsubscribe(this);
// 			it++;
// 		}
// 	}
// }


void Necromancer::attach(Unit* observable) {
	std::cout << "Atach method\n";

	if ( getUnitType() != NECROMANCER ) { return; }

	m_observables.insert(observable);
	observable->subscribe(this);

	printObservables();
}
void Necromancer::detach(Unit* observable) {
	std::cout << "Detach method\n";

	if ( getUnitType() != NECROMANCER ) { return; }

	m_observables.erase(observable);
	observable->unsubscribe(this); // нужно ли отписывать, если юнит и так мертв?

	printObservables();
}
void Necromancer::update(Unit* observable) {
	if ( getUnitType() != NECROMANCER ) { return; }

	int hp = observable->getMaxHitPoints() * 0.2;

	std::cout << hp << std::endl;

	addHitPoints(hp);

	detach(observable);
}


void Necromancer::printObservables() const {
	if ( !m_observables.empty() ) {
		std::cout << "Observables:\n";
		
		auto it = m_observables.begin();

		while ( it != m_observables.end() ) {
			std::cout << (*it)->getName() << std::endl;
			it++;
		}
	} else {
		std::cout << "Observables: empty\n";
	}
}