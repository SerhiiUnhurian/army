all: army
 
army: main.o State.o Unit.o Attack.o Soldier.o Rogue.o RogueAttack.o Vampire.o VampireAttack.o Ability.o VampireAbility.o WerewolfAbility.o WolfState.o Werewolf.o Wizard.o MagicState.o SpellBook.o Spell.o Fireball.o FlashHeal.o SpellCaster.o Berserker.o BerserkerState.o Priest.o PriestAttack.o Healer.o Warlock.o WarlockAbility.o Demon.o Necromancer.o NecromancerAttack.o
	g++ *.o -o main.exe 

main.o: main.cpp 
		g++ -c main.cpp

Unit.o: Units/Unit.cpp 
		g++ -c Units/Unit.cpp

State.o: States/State.cpp 
		g++ -c States/State.cpp

Attack.o: Attacks/Attack.cpp
		g++ -c Attacks/Attack.cpp

Soldier.o: Units/Soldier.cpp
		g++ -c Units/Soldier.cpp

Rogue.o: Units/Rogue.cpp
		g++ -c Units/Rogue.cpp

RogueAttack.o: Attacks/RogueAttack.cpp
		g++ -c Attacks/RogueAttack.cpp

Vampire.o: Units/Vampire.cpp
		g++ -c Units/Vampire.cpp

VampireAttack.o: Attacks/VampireAttack.cpp
		g++ -c Attacks/VampireAttack.cpp

Ability.o: Ability/Ability.cpp
		g++ -c Ability/Ability.cpp

VampireAbility.o: Ability/VampireAbility.cpp
		g++ -c Ability/VampireAbility.cpp

WerewolfAbility.o: Ability/WerewolfAbility.cpp
		g++ -c Ability/WerewolfAbility.cpp

WolfState.o: States/WolfState.cpp
		g++ -c States/WolfState.cpp

Werewolf.o: Units/Werewolf.cpp
		g++ -c Units/Werewolf.cpp

Wizard.o: Spellcasters/Wizard.cpp
		g++ -c Spellcasters/Wizard.cpp

MagicState.o: States/MagicState.cpp
		g++ -c States/MagicState.cpp

SpellBook.o: Spell/SpellBook.cpp
		g++ -c Spell/SpellBook.cpp

Spell.o: Spell/Spell.cpp
		g++ -c Spell/Spell.cpp

Fireball.o: Spell/Fireball.cpp
		g++ -c Spell/Fireball.cpp

FlashHeal.o: Spell/FlashHeal.cpp
		g++ -c Spell/FlashHeal.cpp
		
SpellCaster.o: Spellcasters/SpellCaster.cpp
		g++ -c Spellcasters/SpellCaster.cpp

Berserker.o: Units/Berserker.cpp
		g++ -c Units/Berserker.cpp

BerserkerState.o: States/BerserkerState.cpp
		g++ -c States/BerserkerState.cpp

Priest.o: Spellcasters/Priest.cpp
		g++ -c Spellcasters/Priest.cpp

PriestAttack.o: Attacks/PriestAttack.cpp
		g++ -c Attacks/PriestAttack.cpp

Healer.o: Spellcasters/Healer.cpp
		g++ -c Spellcasters/Healer.cpp

Warlock.o: Spellcasters/Warlock.cpp
		g++ -c Spellcasters/Warlock.cpp

WarlockAbility.o: Ability/WarlockAbility.cpp
		g++ -c Ability/WarlockAbility.cpp

Demon.o: Units/Demon.cpp
		g++ -c Units/Demon.cpp

Necromancer.o: Spellcasters/Necromancer.cpp
		g++ -c Spellcasters/Necromancer.cpp

NecromancerAttack.o: Attacks/NecromancerAttack.cpp
		g++ -c Attacks/NecromancerAttack.cpp

clean: 
	rm -rf *.o main.exe

