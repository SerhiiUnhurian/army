#include "Units/Soldier.h"
#include "Units/Rogue.h"
#include "Units/Unit.h"
#include "Units/Vampire.h"
#include "Units/Werewolf.h"
#include "Spellcasters/Warlock.h"
#include "Spellcasters/Necromancer.h"
#include "Units/Demon.h"

#include "Units/Properties.h"
#include "Units/Exceptions.h"
#include <iostream>


#include "States/MagicState.h"
#include "Spellcasters/SpellCaster.h"

int main() {
	Soldier *s1 = new Soldier("Soldier", SOLDIER);


	Unit *u2 = Unit::createUnit("Warlock", WARLOCK);
	Warlock* wa = dynamic_cast<Warlock*>(u2);

	Unit *u3 = Unit::createUnit("Necromancer", NECROMANCER);
	Necromancer* nec = dynamic_cast<Necromancer*>(u3);


	std::cout << s1 << std::endl;
	s1->printObservers();

	nec->attack(wa);
	wa->attack(nec);
	s1->attack(nec);
	s1->attack(nec);
	s1->attack(wa);
	s1->attack(wa);
	s1->attack(wa);
	s1->attack(wa);
	nec->cast(FIREBALL, s1);

	std::cout << nec << std::endl;
	nec->printObservables();
	
	std::cout << wa << std::endl;
	wa->printObservers();

	std::cout << s1 << std::endl;
	s1->printObservers();

	return 0;
}
