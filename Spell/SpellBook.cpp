#include "SpellBook.h"
#include "Fireball.h"
#include "FlashHeal.h"
#include "../States/MagicState.h"
#include "../Units/Properties.h"
#include "SpellBook.h"
#include <map>

class Spell;

SpellBook::SpellBook() {

	m_book.insert(std::pair<SpellName, Spell*>(FIREBALL, new Fireball()));
	m_book.insert(std::pair<SpellName, Spell*>(FLASH_HEAL, new FlashHeal()));

	// book.at(FIREBALL) = Fireball();
	// book.at(FLASH_HEAL) = FlashHeal();

	// book.emplace(FIREBALL, Fireball());
	// book.emplace(FLASH_HEAL, FlashHeal());
}

SpellBook::~SpellBook() {} // нужно ли здесь что-либо удалять?

SpellBook* SpellBook::getSpellBook() {
	if ( s_spellBook == nullptr ) {
		s_spellBook = new SpellBook();
	}
	return s_spellBook;
}

Spell* SpellBook::getSpell(SpellName spell) const {
	return m_book.at(spell);
}

std::ostream& operator<<(std::ostream& out, const SpellBook* spellBook) {
	auto it { spellBook->m_book.begin() };

	std::cout << "--Spell Book--" << std::endl;

	while (it != spellBook->m_book.end())
	{
		std::cout << it->second << std::endl; // find out how it works
		it++;
	}
	std::cout << "--End of Spell Book--" << std::endl;
}

SpellBook* SpellBook::s_spellBook = nullptr;
