#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

class OutOfHpException {};
class OutOfManaException {};
class NotAllowedException {};
class InvalidTargetException {};

#endif // EXCEPTIONS_H